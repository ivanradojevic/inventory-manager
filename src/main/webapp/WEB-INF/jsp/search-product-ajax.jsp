<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>

<link rel="stylesheet" type="text/css" href="/productTableCSS.css">
<link rel="stylesheet" type="text/css" href="productTableCSS.css">
<meta charset="UTF-8" />
<title>Products Ajax Search</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	
</head>
<body>

	Search products by name:<input type="text" id="searchTerm" name="searchTerm" onkeyup="searchProducts()" />

	<br>
	<br>
	

<div id="products-table"></div>

</body>

<script type="text/javascript">
	function searchProducts() {

		$.ajax({
			url : '/products/search',
			data : {
				term : $('#searchTerm').val(),
				ajax : 'true'
			},
			success : function(data) {
				$('#products-table').html(data);
			}
		});
	}
</script>
</html>