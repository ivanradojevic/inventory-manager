<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<meta charset="UTF-8" />
<title>Examples</title>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Employee Types</h1>
	<br>
	<br>
	<div>
		<table>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>description</th>
			</tr>
			<c:forEach items="${employeeTypes}" var="employeeType">
				<tr>
					<td>${employeeType.id}</td>
					<td>${employeeType.name}</td>
					<td>${employeeType.description}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>