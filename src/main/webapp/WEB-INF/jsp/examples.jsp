<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta charset="UTF-8" />
<title>Examples</title>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>
<body>

	<h1>Examples</h1>
	<br />
	<a href="/add-example/" id="manual-ajax-add" class="manual-ajax-add"><button>Add
			Example</button></a>
	<div>
		<table border="1">
			<thead>
				<tr>
					<th scope="col">id</th>
					<th scope="col">name</th>
				</tr>
			</thead>
			<c:forEach items="${examples}" var="example">
				<tr>
					<td>${example.id}</td>
					<td>${example.name}</td>
					<td><a href="/edit-example/${example.id}" id="manual-ajax-edit"
						class="manual-ajax-edit"><button>Edit Example</button></a></td>
					<td><a href="/delete-example/${example.id}" id="manual-ajax-delete"
						class="manual-ajax-delete"><button>Delete Example</button></a></td>
					<td><a href="/search/${example.id}"
						id="manual-ajax-${example.id}" class="manual-ajax">Details</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>

	<div class="modal" id="details"></div>


</body>
<script>
	$('.manual-ajax').click(function(event) {
		event.preventDefault();
		this.blur(); // Manually remove focus from clicked link.
		$.get(this.href, function(html) {
			$(html).appendTo('body').modal();
		});
	});

	$('.manual-ajax-add').click(function(event) {
		event.preventDefault();
		this.blur(); // Manually remove focus from clicked link.
		$.get(this.href, function(html) {
			$(html).appendTo('body').modal();
		});
	});

	$('.manual-ajax-edit').click(function(event) {
		event.preventDefault();
		this.blur(); // Manually remove focus from clicked link.
		$.get(this.href, function(html) {
			$(html).appendTo('body').modal();
		});
	});
	
	$('.manual-ajax-delete').click(function(event) {
		event.preventDefault();
		this.blur(); // Manually remove focus from clicked link.
		$.get(this.href, function(html) {
			$(html).appendTo('body').modal();
		});
	});
</script>