<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employees Ajax Search</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
</head>
<body>

	Search employees by name:
	<input type="text" id="searchTerm" name="searchTerm"
		onkeyup="searchEmployees()" />


	<div id="employees-table"></div>
</body>

<script type="text/javascript">
	function searchEmployees() {

		$.ajax({
			url : '/employees/search-employees-ajax',
			data : {
				term : $('#searchTerm').val(),
				ajax : 'true'
			},
			success : function(data) {
				$('#employees-table').html(data);
			}
		});
	}
</script>

</html>