<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="productTableCSS.css">
<meta charset="UTF-8" />
<title>Products</title>

</head>

<body>
	<h1>Products</h1>
	<br />
	<br />
	<div>
		<table id="products">
			<tr>
				<th>id</th>
				<th>name</th>
				<th>serial number</th>
				<th>product number</th>
				<th>product type</th>
				<th>manufacturer</th>
			</tr>
			<c:forEach items="${products}" var="product">
				<tr>
					<td>${product.id}</td>
					<td>${product.name}</td>
					<td>${product.sN}</td>
					<td>${product.pN}</td>
					<td>${product.productType.name}</td>
					<td>${product.manufacturer.name}</td>


				</tr>
			</c:forEach>
		</table>
	</div>
</body>



</html>