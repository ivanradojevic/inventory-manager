<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

      <table border="1">
        <tr>
          <th>id</th>
          <th>name</th>
        </tr>
        <c:forEach  items="${examples}" var ="example">
        <tr>
          <td>${example.id}</td>
          <td>${example.name}</td>
        </tr>
        </c:forEach>
      </table>
  