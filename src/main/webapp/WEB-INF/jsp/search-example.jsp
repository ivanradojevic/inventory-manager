<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form:form action="search" modelAttribute="example" method="POST">
      <table>
          <tr>
              <td>Example name search:</td>
              <td><form:input path="name"/></td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="Search" />
              </td>
          </tr>
      </table>
  </form:form>