<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<form:form action="save-edit-product" modelAttribute="product"
	method="POST">
	<table>

		<c:set var="tt" value="tt22" scope="session" />
		<tr>
			<td>Product ID:</td>
			<td><form:input path="id" /></td>
		</tr>
		<tr>
			<td>Product Name:</td>
			<td><form:input path="name" /> <form:errors path="name"
					cssStyle="color: #ff0000;" /></td>
		</tr>
		<tr>
			<td>Serial Number:</td>
			<td><form:input path="sN" /> <form:errors path="sN"
					cssStyle="color: #ff0000;" /></td>
		</tr>
		<tr>
			<td>Product Number:</td>
			<td><form:input path="pN" /> <form:errors path="pN"
					cssStyle="color: #ff0000;" /></td>
		</tr>

		<tr>

			<td><form:label path="manufacturer.id">Manufacturer </form:label></td>

			<td><form:select multiple="single" path="manufacturer.id">
					<form:option value="-1" label="Select" />
					<form:options items="${manufacturerList}" />
				</form:select></td>

		</tr>


		<tr>
			<td><form:label path="productType.id">ProductType ID</form:label></td>

			<td><form:select multiple="single" path="productType.id">
					<form:option value="-1" label="Select" />
					<form:options items="${productTypeList}" />
				</form:select></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Save Changes" /></td>
		</tr>
	</table>


	<c:if test="${productSaved}">
		<script type="text/javascript">
			alert('The product was successfully saved!');
		</script>
	</c:if>
</form:form>