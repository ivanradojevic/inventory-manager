<!DOCTYPE HTML>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Examples Ajax</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
  </head>
  <body>
  
  search term: <input type="text" id="searchTerm" name="searchTerm" onkeyup="searchExamples()"/>
  
  <br/>
  
		

 <div id="examples-table"></div>

</body>

<script type="text/javascript">
    function searchExamples() {
    
        $.ajax({
            url : '/search-example-name',
            data: {
                name: $('#searchTerm').val(),
                ajax: 'true'
            },
            success : function(data) {
                $('#examples-table').html(data);
            }
        });
    }
    
  
</script>
</html>