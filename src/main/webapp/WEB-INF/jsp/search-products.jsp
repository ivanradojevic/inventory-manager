<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<html>
<head>
<link rel="stylesheet" type="text/css" href="/productTableCSS.css">
</head>

<body>

	<form:form action="/products/search" modelAttribute="productSearchBean" >
		<table >
			<tr>
				<td>Searching products by term:</td>
				<td><form:input path="term" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Search" /></td>
			</tr>
		</table>
	</form:form>
	<c:if test="${not empty products}">
		<div>

			<table id="products">
				<tr>
					<th>id</th>
					<th>name</th>
					<th>serial number</th>
					<th>product number</th>
					<th>product type</th>
					<th>manufacturer</th>
				</tr>
				<c:forEach items="${products}" var="product">
					<tr>
						<td>${product.id}</td>
						<td>${product.name}</td>
						<td>${product.sN}</td>
						<td>${product.pN}</td>
						<td>${product.productType.name}</td>
						<td>${product.manufacturer.name}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>

</body>
</html>