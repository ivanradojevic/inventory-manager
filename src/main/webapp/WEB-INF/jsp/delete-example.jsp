<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form:form action="delete-example" modelAttribute="example" method="POST">
      <table>
      	  <tr>
              <td>Example Id:</td>
              <td><form:input path="id" readonly="true" /></td>
          </tr>
          <tr>
              <td>Example Name:</td>
              <td><form:input path="name"  readonly="true" /></td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="Delete" />
              </td>
          </tr>
      </table>
   
      <c:if test="${exampleDeleted}">
	       <script type="text/javascript">
	  			alert('The Example was successfully deleted!');
	  	   </script>
      </c:if>
  </form:form>