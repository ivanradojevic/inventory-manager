<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form:form action="save-example" modelAttribute="example">
      <table>
      	  <tr>
              <td>Example Id:</td>
              <td><form:input path="id" readonly="id != null" /></td>
          </tr>
          <tr>
              <td>Example Name:</td>
              <td><form:input path="name" /><form:errors path="name" cssStyle="color: #ff0000;"/></td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="Save Changes" />
              </td>
          </tr>
      </table>
   
      <c:if test="${exampleSaved}">
	       <script type="text/javascript">
	  			alert('The example was successfully saved!');
	  	   </script>
      </c:if>
  </form:form>