<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee</title>
</head>
<body>
	<form:form action="save-employee" modelAttribute="employee">
		<table>
			<tr>
				<td>Employee Id:</td>
				<td><form:input path="id" readonly="true" /></td>
			</tr>
			<tr>
				<td>Employee first name:</td>
				<td><form:input path="firstName" /> <form:errors
						path="firstName" cssStyle="color: #ff0000;" /></td>
			</tr>
			<tr>
				<td>Employee last name:</td>
				<td><form:input path="lastName" /> <form:errors
						path="lastName" cssStyle="color: #ff0000;" /></td>
			</tr>
			<tr>
				<td>Employee type:</td>

				<td><form:select path="employeeTypeId">
						<form:options items="${employeeTypes}" />
					</form:select></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Save Changes" /></td>
			</tr>
		</table>

		<c:if test="${employeeSaved}">
			<script type="text/javascript">
				alert('The employee was successfully saved!');
			</script>
		</c:if>
	</form:form>
</body>
</html>