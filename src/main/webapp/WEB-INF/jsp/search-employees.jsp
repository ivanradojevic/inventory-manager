<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search employees</title>
</head>
<body>
	<h1>Employees</h1>

	<br>
	<div id="searchForm">
		<table>
			<tbody>
				<form:form action="search" modelAttribute="employeeSearchBean">
					<tr>
						<td>Search term <form:input path="term" /> <form:errors
								path="term" cssStyle="error" /></td>
					</tr>
					<tr>
						<td><button>Search</button></td>
					</tr>
				</form:form>
			</tbody>
		</table>
	</div>
	<c:choose>
		<c:when test="${not empty employees}">
			<div id="data">
				<h2>Results</h2>
				<jsp:include page="common/employees-table.jsp" />
			</div>
		</c:when>
		<c:otherwise>
			<div id="empty">
				<h2>No results by search criteria.</h2>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>