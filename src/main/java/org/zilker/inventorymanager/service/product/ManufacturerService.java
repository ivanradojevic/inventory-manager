package org.zilker.inventorymanager.service.product;

import java.util.List;

import org.zilker.inventorymanager.model.product.Manufacturer;

public interface ManufacturerService {
	
	Manufacturer findOne(Long id);

	List<Manufacturer> findAll();

	Manufacturer save(Manufacturer manufacturer);

	Manufacturer delete(Long id);

}
