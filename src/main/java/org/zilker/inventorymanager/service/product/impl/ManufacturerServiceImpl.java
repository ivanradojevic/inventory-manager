package org.zilker.inventorymanager.service.product.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.product.Manufacturer;
import org.zilker.inventorymanager.repository.product.ManufacturerRepository;
import org.zilker.inventorymanager.service.product.ManufacturerService;

@Service
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService {

	@Autowired
	private ManufacturerRepository manufacturerRepository;
	
	@Override
	public Manufacturer findOne(Long id) {
		
		return manufacturerRepository.findOne(id);
	}

	@Override
	public List<Manufacturer> findAll() {
		
		return manufacturerRepository.findAll();
	}

	@Override
	public Manufacturer save(Manufacturer manufacturer) {
		
		return manufacturerRepository.save(manufacturer);
	}

	@Override
	public Manufacturer delete(Long id) {

		Manufacturer manufacturer = manufacturerRepository.findOne(id);
		if(manufacturer != null){
			manufacturerRepository.delete(manufacturer);
		}
		
		return manufacturer;
	}
	

}
