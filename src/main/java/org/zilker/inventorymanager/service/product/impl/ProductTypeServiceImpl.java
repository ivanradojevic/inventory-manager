package org.zilker.inventorymanager.service.product.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.product.ProductType;
import org.zilker.inventorymanager.repository.product.ProductTypeRepository;
import org.zilker.inventorymanager.service.product.ProductTypeService;


@Service
@Transactional
public class ProductTypeServiceImpl implements ProductTypeService {

	
	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Override
	public ProductType findOne(Long id) {
		return productTypeRepository.findOne(id);
	}

	@Override
	public List<ProductType> findAll() {
		return productTypeRepository.findAll();
	}

	@Override
	public ProductType save(ProductType productType) {

		return productTypeRepository.save(productType);
	}

	@Override
	public ProductType delete(Long id) {
		ProductType productType = productTypeRepository.findOne(id);
		if(productType != null){
			productTypeRepository.delete(productType);
		}
		
		return productType;
	}

}
