package org.zilker.inventorymanager.service.product;

import java.util.List;

import org.zilker.inventorymanager.model.product.ProductType;

public interface ProductTypeService {
	
	ProductType findOne(Long id);

	List<ProductType> findAll();

	ProductType save(ProductType productType);

	ProductType delete(Long id);
	
	
}
