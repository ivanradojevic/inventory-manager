package org.zilker.inventorymanager.service.product;

import java.util.List;

import org.zilker.inventorymanager.model.product.Product;



public interface ProductService {
	
	Product findOne(Long id);

	List<Product> findAll();

	List<Product> search(String name);
	
	Product save(Product product) throws Exception;

	Product delete(Long id);
	
}
