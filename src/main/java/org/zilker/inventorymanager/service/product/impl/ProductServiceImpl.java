package org.zilker.inventorymanager.service.product.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.product.Manufacturer;
import org.zilker.inventorymanager.model.product.Product;
import org.zilker.inventorymanager.model.product.ProductType;
import org.zilker.inventorymanager.repository.product.ManufacturerRepository;
import org.zilker.inventorymanager.repository.product.ProductRepository;
import org.zilker.inventorymanager.repository.product.ProductTypeRepository;
import org.zilker.inventorymanager.service.product.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService{
	
	private Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductTypeRepository productTypeRepository;
	
	@Autowired
	private ManufacturerRepository manufacturerRepository;
	
	@Override
	public Product findOne(Long id) {
		return productRepository.findOne(id);
	}

	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}
	
	@Override
	public Product save(Product product) throws Exception {
		
		log.debug("save entered for product {}", product.getpN());
		
		Long manufacturerId = product.getManufacturerId() != null ? product.getManufacturerId() : product.getManufacturer().getId();
		Long productTypeId = product.getProductTypeId() != null ? product.getProductTypeId() : product.getProductType().getId();
		
		Manufacturer manufacturer = manufacturerRepository.findOne(manufacturerId);
		if (manufacturer == null) {
			// add log error with message
			throw new Exception("manufacturer does not exist for id: " + manufacturerId);
		}
		
		ProductType productType = productTypeRepository.findOne(productTypeId);
		if (productType == null) {
			// add log error with message
			throw new Exception("productType does not exist for id: " + productTypeId);
		}
		
		product.setManufacturer(manufacturerRepository.findOne(manufacturerId));
		product.setProductType(productTypeRepository.findOne(productTypeId));
		
		log.debug("save finished for product {}", product.getpN());
				
		return productRepository.save(product);
	}

	@Override
	public Product delete(Long id) {
		Product product = productRepository.findOne(id);
		if(product != null){
			productRepository.delete(product);
		}
		
		return product;
	}

	@Override
	public List<Product> search(String name) {
		
        if(name != null) {
        	name = '%' + name + '%';
        	name = '%' + name.toLowerCase() + '%';
        }
		return productRepository.search(name);
	}
	
		
}
