package org.zilker.inventorymanager.service.employee;

import java.util.List;

import org.zilker.inventorymanager.model.employee.EmployeeType;

public interface EmployeeTypeService {
	EmployeeType findOne(Long id);

	EmployeeType save(EmployeeType employeeType);

	List<EmployeeType> findAll();

	EmployeeType delete(Long id);
}
