package org.zilker.inventorymanager.service.employee;

import java.util.List;

import org.zilker.inventorymanager.model.employee.Employee;

public interface EmployeeService {
	Employee findOne(Long id);
	
	Employee save(Employee employee);
	
//	List<Employee> search(String firstName, String lastName);
	
	List<Employee> search(String searchTerm);
	
	List<Employee> findAll();
	
	Employee delete(Long id);

	List<Employee> findAllByName(String name);
}
