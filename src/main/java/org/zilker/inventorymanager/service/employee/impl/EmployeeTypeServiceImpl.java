package org.zilker.inventorymanager.service.employee.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.employee.EmployeeType;
import org.zilker.inventorymanager.repository.employee.EmployeeTypeRepository;
import org.zilker.inventorymanager.service.employee.EmployeeTypeService;

@Service
@Transactional
public class EmployeeTypeServiceImpl implements EmployeeTypeService{
	@Autowired
	private EmployeeTypeRepository employeeTypeRepository;
	
	@Override
	public EmployeeType findOne(Long id) {
		return employeeTypeRepository.findOne(id);
	}

	@Override
	public EmployeeType save(EmployeeType employeeType) {
		return employeeTypeRepository.save(employeeType);
	}

	@Override
	public List<EmployeeType> findAll() {
		return employeeTypeRepository.findAll();
	}

	@Override
	public EmployeeType delete(Long id) {
		EmployeeType employeeType = findOne(id);
		if(employeeType != null) {
			employeeTypeRepository.delete(employeeType);
		}
		return employeeType;
	}
}
