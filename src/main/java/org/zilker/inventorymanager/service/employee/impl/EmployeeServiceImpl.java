package org.zilker.inventorymanager.service.employee.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.employee.Employee;
import org.zilker.inventorymanager.model.employee.EmployeeType;
import org.zilker.inventorymanager.repository.employee.EmployeeRepository;
import org.zilker.inventorymanager.repository.employee.EmployeeTypeRepository;
import org.zilker.inventorymanager.service.employee.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeTypeRepository employeeTypeRepository;

	@Override
	public Employee findOne(Long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public Employee save(Employee employee) {
		EmployeeType employeeType = employeeTypeRepository.findOne(employee.getEmployeeTypeId());
		employee.setEmployeeType(employeeType);
		return employeeRepository.save(employee);
	}

	@Override
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee delete(Long id) {
		Employee employee = findOne(id);
		if (employee != null) {
			employeeRepository.delete(employee);
		}
		return employee;
	}

	@Override
	public List<Employee> search(String searchTerm) {

		if (searchTerm != null) {
			searchTerm = '%' + searchTerm.toLowerCase() + '%';
		}

		return employeeRepository.search(searchTerm);
	}

	@Override
	public List<Employee> findAllByName(String name) {
		if (name != null) {
			name = '%' + name.toLowerCase() + '%';
		}
		return employeeRepository.search(name);
	}

}
