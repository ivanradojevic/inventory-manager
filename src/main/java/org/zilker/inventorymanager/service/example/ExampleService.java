package org.zilker.inventorymanager.service.example;

import java.util.List;

import org.zilker.inventorymanager.model.example.Example;

public interface ExampleService {

	Example findOne(Long id);

	List<Example> findAll();
	
	List<Example> findAllByName(String name);

	Example save(Example example);

	Example delete(Long id);
	
}