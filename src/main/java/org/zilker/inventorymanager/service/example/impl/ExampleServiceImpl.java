package org.zilker.inventorymanager.service.example.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zilker.inventorymanager.model.example.Example;
import org.zilker.inventorymanager.repository.example.ExampleRepository;
import org.zilker.inventorymanager.service.example.ExampleService;

@Service
@Transactional
public class ExampleServiceImpl implements ExampleService {

	@Autowired
	private ExampleRepository exampleRepository;
	
	
	@Override
	public Example findOne(Long id) {
		return exampleRepository.findOne(id);
	}

	@Override
	public List<Example> findAll() {
		return exampleRepository.findAll();
	}
	
	@Override
	public Example save(Example example) {
		return exampleRepository.save(example);
	}

	@Override
	public Example delete(Long id) {
		Example example = exampleRepository.findOne(id);
		if(example != null){
			exampleRepository.delete(example);
		}
		
		return example;
	}

	@Override
	public List<Example> findAllByName(String name) {
		return exampleRepository.findByNameContaining(name);
	}

	
}