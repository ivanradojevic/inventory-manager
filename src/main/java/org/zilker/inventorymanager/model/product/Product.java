package org.zilker.inventorymanager.model.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Product {
	
	@Id
	@GeneratedValue
	@Column
	private Long id;	
	@Column(nullable=false)
	@NotBlank(message = "Name is mandatory")
	private String name;
	@Column(nullable=false)
	private String sN;
	@Column(nullable=false)
	private String pN;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private ProductType productType;
	@ManyToOne(fetch=FetchType.EAGER)
	private Manufacturer manufacturer;
	@Transient
	private Long manufacturerId;
	@Transient
	private Long productTypeId;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}

	public Long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	
	
	
	public ProductType getProductType() {
		return productType;
	}
	
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getsN() {
		return sN;
	}
	public void setsN(String sN) {
		this.sN = sN;
	}
	public String getpN() {
		return pN;
	}
	public void setpN(String pN) {
		this.pN = pN;
	}
	
	

}
