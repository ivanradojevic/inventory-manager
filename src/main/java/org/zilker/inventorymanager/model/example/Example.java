package org.zilker.inventorymanager.model.example;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Example{

	@Id
	@GeneratedValue
	@Column
	private Long id;

	@Column(nullable=false)
	@NotBlank(message = "Name is mandatory")
	private String name;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	
}