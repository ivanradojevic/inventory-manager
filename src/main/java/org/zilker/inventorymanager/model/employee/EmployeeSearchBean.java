package org.zilker.inventorymanager.model.employee;

public class EmployeeSearchBean {
	
	public EmployeeSearchBean() {
	}
	
	public EmployeeSearchBean(String term) {
		super();
		this.term = term;
	}

	String term;

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	
	
}
