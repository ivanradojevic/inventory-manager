package org.zilker.inventorymanager.repository.employee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.zilker.inventorymanager.model.employee.EmployeeType;

@Repository
public interface EmployeeTypeRepository  extends JpaRepository<EmployeeType, Long>{

}
