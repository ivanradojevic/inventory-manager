package org.zilker.inventorymanager.repository.employee;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.zilker.inventorymanager.model.employee.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("SELECT e FROM Employee e WHERE " + "(:searchTerm IS NULL OR LOWER(e.firstName) like :searchTerm) OR "
			+ "(LOWER(e.lastName) like :searchTerm) ")

	List<Employee> search(@Param("searchTerm") String searchTerm);

	@Query("SELECT e FROM Employee e WHERE " + "(:firstName IS NULL OR LOWER(e.firstName) like :firstName) AND "
			+ "(:lastName IS NULL OR LOWER(e.lastName) like :lastName) ")
	
	List<Employee> search(@Param("firstName") String firstName, @Param("lastName") String lastName);

}
