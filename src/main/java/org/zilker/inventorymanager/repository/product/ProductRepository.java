package org.zilker.inventorymanager.repository.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.zilker.inventorymanager.model.product.Product;


@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
		
	@Query("SELECT p FROM Product p WHERE "
			+ "(:name IS NULL OR LOWER(p.name) like :name"
			+ " OR LOWER(p.sN) like :name "
			+ "OR LOWER(p.pN) like :name)" )
	List<Product> search(@Param("name") String name);

}
