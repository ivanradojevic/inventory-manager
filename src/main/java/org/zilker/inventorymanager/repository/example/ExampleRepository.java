package org.zilker.inventorymanager.repository.example;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.zilker.inventorymanager.model.example.Example;


@Repository
public interface ExampleRepository extends JpaRepository<Example, Long> {
		
	List<Example> findByNameContaining(String name);
}