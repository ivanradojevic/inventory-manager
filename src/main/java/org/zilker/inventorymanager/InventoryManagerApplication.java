package org.zilker.inventorymanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication
public class InventoryManagerApplication 
	extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InventoryManagerApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(InventoryManagerApplication.class, args);
	}

}
