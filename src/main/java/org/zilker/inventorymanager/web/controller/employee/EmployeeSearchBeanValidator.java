package org.zilker.inventorymanager.web.controller.employee;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.zilker.inventorymanager.model.employee.EmployeeSearchBean;

@Component
public class EmployeeSearchBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EmployeeSearchBean.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		// check if search search term is empty
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "term", "required.employeeSearchField",
				"No search term");

		// check if search term is at least 2 chars long
		EmployeeSearchBean employeeSearchBean = (EmployeeSearchBean) target;
		if (employeeSearchBean != null && employeeSearchBean.getTerm().length() < 2
				&& !employeeSearchBean.getTerm().isEmpty()) {
			errors.rejectValue("term", "required.short.employeeSearchField", "Less then 2 characters");
		}
	}
}