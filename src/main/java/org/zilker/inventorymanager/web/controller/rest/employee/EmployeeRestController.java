package org.zilker.inventorymanager.web.controller.rest.employee;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.employee.Employee;
import org.zilker.inventorymanager.service.employee.EmployeeService;

@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeRestController {
	@Autowired
	private EmployeeService employeeService;

	private Logger log = LoggerFactory.getLogger(EmployeeRestController.class);

	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<Employee>> getAllEmployees() {

		log.debug("getAllEmployees method entered!");
		List<Employee> employees = employeeService.findAll();
		log.debug("getAllEmployees method finished!");
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
		log.debug("getEmployeeById method entered for id {}!", id);
		Employee employee = employeeService.findOne(id);
		if (employee == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		log.debug("getEmployeeById method finished for id {}!", id);

		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<Employee> deleteEmployee(@PathVariable Long id) {
		log.debug("deleteEmployee method entered for id {}!", id);

		Employee deleted = employeeService.delete(id);
		if (deleted == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		log.debug("deleteEmployee method finished for id {}!", id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Employee> addEmployee(@Validated @RequestBody Employee employee) {
		log.debug("addEmployee entered for example {}!",
				employee.getId() + " " + employee.getFirstName() + " " + employee.getLastName());
		Employee newEmployee = employee;

		employeeService.save(newEmployee);

		log.debug("addEmployee finished for example {}!",
				employee.getId() + " " + employee.getFirstName() + " " + employee.getLastName());

		return new ResponseEntity<>(newEmployee, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<Employee> editEmployee(@Validated @RequestBody Employee employee, @PathVariable Long id) {
		log.debug("editEmployee entered for example {}!", employee.getId());

		if (!id.equals(employee.getId())) {
			log.warn("ids do not match {} and {}!", employee.getId(), id);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Employee persistedEmployee = employeeService.save(employee);
		
		log.debug("editEmployee finished for example {}!", employee.getId());
		return new ResponseEntity<>(persistedEmployee, HttpStatus.OK);
	}


	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Exception> handle(Exception e) {
		log.error("Unexpected error occurred: ", e);

		// TODO
		// create error response object, instead of exception
		return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
