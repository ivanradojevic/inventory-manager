package org.zilker.inventorymanager.web.controller.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.zilker.inventorymanager.model.product.Manufacturer;
import org.zilker.inventorymanager.model.product.Product;
import org.zilker.inventorymanager.model.product.ProductType;
import org.zilker.inventorymanager.service.product.ManufacturerService;
import org.zilker.inventorymanager.service.product.ProductService;
import org.zilker.inventorymanager.service.product.ProductTypeService;
import org.zilker.inventorymanager.web.controller.product.beans.ProductSearchBean;

@Controller
@RequestMapping(value = "/products")
public class ProductController {

	@Autowired
	private ManufacturerService manufacturerService;

	@Autowired
	private ProductTypeService productTypeService;

//	@Autowired
//	@Qualifier("productValidator")
//	private ProductValidator productValidator;
//
//	@InitBinder
//	private void initBinder(WebDataBinder binder) {
//		binder.setValidator(productValidator);
//	}

	@Autowired
	private ProductService productService;

	private Logger log = LoggerFactory.getLogger(ProductController.class);

	@GetMapping(value = "/search-form")
	String dispaySearchForm(@ModelAttribute("productSearchBean") ProductSearchBean productSearchBean) throws Exception {
		return "search-products";
	}

	@RequestMapping(value = "/search-form-ajax")
	public String dispaySearchFormAjax(Model model, @ModelAttribute ProductSearchBean product) throws Exception {

		return "search-product-ajax";

	}

	/**
	 * Executes search products from non-ajax page
	 * 
	 * @param model
	 * @param productSearchBean
	 * @param result
	 * @return
	 * @throws Exception
	 */

	@PostMapping(value = "/search")
	String searchProducts(Model model, @ModelAttribute @Valid ProductSearchBean productSearchBean, BindingResult result)
			throws Exception {

		if (result.hasErrors()) {
			log.warn("searchProducts validation errors: " + result);
			return "search-product";
		}

		return searchProducts(model, productSearchBean, false);
	}

	/**
	 * Executes search products from AJAX page
	 * 
	 * @param model
	 * @param term
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = "/search")
	String searchProducts(Model model, @RequestParam(value = "term", required = false) String term) throws Exception {

		ProductSearchBean productSearchBean = new ProductSearchBean(term);

		return searchProducts(model, productSearchBean, true);
	}

	private String searchProducts(Model model, ProductSearchBean productSearchBean, boolean ajax) {
		List<Product> products = productService.search(productSearchBean.getTerm());

		model.addAttribute("products", products);

		return ajax ? "products-table-ajax" : "search-products";

	}

	@GetMapping(value = "/save")
	public String showSavingForm(Model model, @RequestParam(value = "id", required = false) Long id) throws Exception {

		log.debug("showSavingForm method entered!");

		Product product = new Product();

		if (id != null) {
			product = productService.findOne(id);
		}	
	
		model.addAttribute("product", product);

		return "add-product";

	}

	@PostMapping(value = "/save-edit-product")
	public String saveProduct(Model model, @ModelAttribute @Validated Product product, BindingResult result)
			throws Exception {

		log.debug("saveProduct method entered for name: " + product.getName());

		if (result.hasErrors()) {
			log.warn("saveProduct validation errors: " + result.getErrorCount());
			return "add-product";
		}

		Product savedProduct = productService.save(product);
		model.addAttribute("product", savedProduct);
		model.addAttribute("productSaved", true);
		
	
		log.debug("saveProduct method finished! ");

		return "add-product";

	}

	@GetMapping
	String getAllProducts(Model model) throws Exception {

		log.debug("getAllProducts method entered!");

		List<Product> products = productService.findAll();
		model.addAttribute("products", products);

		log.debug("getAllProducts method finished!");

		return "products";

	}

	@ModelAttribute("productTypeList")
	public Map<Long, String> getProductTypes() {
		List<ProductType> List = productTypeService.findAll();

		Map<Long, String> productTypeList = new HashMap<Long, String>();

		for (ProductType productType : List) {
			productTypeList.put(productType.getId(), productType.getName());
		}

		return productTypeList;
	}

	@ModelAttribute("manufacturerList")
	public Map<Long, String> getManufacturers() {
		List<Manufacturer> List = manufacturerService.findAll();
		Map<Long, String> manufacturerList = new HashMap<Long, String>();

		for (Manufacturer manufacturer : List) {
			manufacturerList.put(manufacturer.getId(), manufacturer.getName());
		}

		return manufacturerList;
	}
	

}
