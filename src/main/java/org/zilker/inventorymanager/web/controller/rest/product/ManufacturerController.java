package org.zilker.inventorymanager.web.controller.rest.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.product.Manufacturer;
import org.zilker.inventorymanager.service.product.ManufacturerService;

@RestController
@RequestMapping(value = "/api/manufacturers")
public class ManufacturerController {

	@Autowired
	private ManufacturerService manufacturerService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Manufacturer>> getManufacturers() {

		return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<Manufacturer> getManufacturerById(@PathVariable Long id) {
		Manufacturer manufacturer = manufacturerService.findOne(id);
		if (manufacturer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(manufacturer, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Manufacturer> addManufacturer(@Validated @RequestBody Manufacturer newManufacturer) {

		Manufacturer savedManufacturer = manufacturerService.save(newManufacturer);

		return new ResponseEntity<>(savedManufacturer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<Manufacturer> deleteManufacturer(@PathVariable Long id) {
		Manufacturer deletedManufacturer = manufacturerService.delete(id);

		if (deletedManufacturer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(deletedManufacturer, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<Manufacturer> editManufacturer(@Validated @RequestBody Manufacturer manufacturer,
			@PathVariable Long id) {

		if (!id.equals(manufacturer.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Manufacturer persistedManufacturer = manufacturerService.save(manufacturer);

		return new ResponseEntity<>(persistedManufacturer, HttpStatus.OK);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
