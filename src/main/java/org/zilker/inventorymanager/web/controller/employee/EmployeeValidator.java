package org.zilker.inventorymanager.web.controller.employee;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.zilker.inventorymanager.model.employee.Employee;

@Component
public class EmployeeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Employee.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "Employee firstname empty from validator", "Employee firstname empty from validator");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "Employee lastname empty from validator", "Employee lastname empty from validator");
	}

}
