package org.zilker.inventorymanager.web.controller.rest.example;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.example.Example;
import org.zilker.inventorymanager.service.example.ExampleService;

@RestController
@RequestMapping(value = "/api/example")
public class ExampleRestController {
	@Autowired
	private ExampleService exampleService;

	private Logger log = LoggerFactory.getLogger(ExampleRestController.class);

	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<Example>> getAllExamples() throws Exception {

		log.debug("getAllExamples method entered!");

		List<Example> allExamples = exampleService.findAll();

		log.debug("getAllExamples method finished!");
		
		return new ResponseEntity<>(allExamples, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<Example> getExampleById(@PathVariable Long id) {
		log.debug("getExampleById method entered for id {}!", id);
		
		Example example = exampleService.findOne(id);
		if (example == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		log.debug("getExampleById method finished for id {}!", id);
		
		return new ResponseEntity<>(example, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<Example> deleteExample(@PathVariable Long id) {
		
		log.debug("deleteExample method entered for id {}!", id);
		
		Example deleted = exampleService.delete(id);

		if (deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		log.debug("deleteExample method finished for id {}!", id);
		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Example> addExample(@Validated @RequestBody Example example) {
		
		log.debug("addExample entered for example {}!", example.getName());

		Example savedExample = exampleService.save(example);
		
		log.debug("addExample finished for example {}!", example.getName());

		return new ResponseEntity<>(savedExample, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<Example> editExample(@Validated @RequestBody Example example, @PathVariable Long id) {

		log.debug("editExample entered for example {}!", example.getId());
		
		if (!id.equals(example.getId())) {
			log.warn("ids do not match {} and {}!", example.getId(), id);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Example persisted = exampleService.save(example);
		
		log.debug("editExample finished for example {}!", example.getId());

		return new ResponseEntity<>(persisted, HttpStatus.OK);
	}

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Exception> handle(Exception e) {
		log.error("Unexpected error occurred: ", e);
		
		// TODO
		// create error response object, instead of exception
		return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}