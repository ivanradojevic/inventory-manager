package org.zilker.inventorymanager.web.controller.employee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.zilker.inventorymanager.model.employee.Employee;
import org.zilker.inventorymanager.model.employee.EmployeeSearchBean;
import org.zilker.inventorymanager.model.employee.EmployeeType;
import org.zilker.inventorymanager.service.employee.EmployeeService;
import org.zilker.inventorymanager.service.employee.EmployeeTypeService;

@Controller
@RequestMapping(value = "/employees")
public class EmployeeController {

	private static final String JSP_EMPLOYEES_TABLE = "/common/employees-table";
	private static final String JSP_SEARCH_EMPLOYEE_AJAX = "search-employees-ajax";
	private static final String JSP_SEARCH_EMPLOYEE = "search-employees";

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private EmployeeTypeService employeeTypeService;

	@Autowired
	@Qualifier("employeeSearchBeanValidator")
	private EmployeeSearchBeanValidator employeeSearchBeanValidator;

	@Autowired
	@Qualifier("employeeValidator")
	private EmployeeValidator employeeValidator;

	@InitBinder("employeeSearchBean")
	private void initEmployeeSearchBeanBinder(WebDataBinder binder) {
		binder.setValidator(employeeSearchBeanValidator);
	}
	
	@InitBinder("employee")
	private void initEmployeeBinder(WebDataBinder binder) {
		binder.setValidator(employeeValidator);
	}

	private Logger log = LoggerFactory.getLogger(EmployeeController.class);

	@GetMapping(value = "/search")
	String displayEmployeeSearch(@ModelAttribute("employeeSearchBean") EmployeeSearchBean employeeSearchBean)
			throws Exception {
		return JSP_SEARCH_EMPLOYEE;
	}

	@GetMapping(value = "/search-ajax")
	public String employeesAjaxSearch() throws Exception {
		return JSP_SEARCH_EMPLOYEE_AJAX;
	}

	@PostMapping(value = "/search")
	String searchEmployees(Model model, @ModelAttribute @Valid EmployeeSearchBean employeeSearchBean,
			BindingResult result) throws Exception {

		if (result.hasErrors()) {
			log.warn("searchEmployee validation errors: " + result);
			return JSP_SEARCH_EMPLOYEE;
		}

		return searchEmployees(model, employeeSearchBean, false);
	}

	@GetMapping(value = "/search-employees-ajax")
	public String searchEmployeesAjax(Model model, @RequestParam(value = "term", required = false) String term,
			@RequestParam(value = "ajax", required = false) String ajax) throws Exception {

		EmployeeSearchBean employeeSearchBean = new EmployeeSearchBean(term);

		return searchEmployees(model, employeeSearchBean, true);

	}

	private String searchEmployees(Model model, EmployeeSearchBean employeeSearchBean, boolean ajax) {

		log.debug("searchEmployees method entered for name {}, ajax: {}", employeeSearchBean.getTerm(), ajax);

		List<Employee> employees = employeeService.search(employeeSearchBean.getTerm());

		model.addAttribute("employees", employees);

		log.debug("searchEmployees method finished!");

		// in case of ajax call, render only table content
		return ajax ? JSP_EMPLOYEES_TABLE : JSP_SEARCH_EMPLOYEE;

	}

	@GetMapping(value = "/edit-employee")
	public String showEmployeeForm(Model model, @RequestParam(value = "id", required = false) Long id)
			throws Exception {

		log.debug("showEmployeeForm method entered!");

		// action="/spring-mvc-java/addEmployee";

		Employee employee = new Employee();
		List<EmployeeType> employeeTypes = employeeTypeService.findAll();
		Map<Long, String> employeeTypesMap = new HashMap<Long, String>();
		for (EmployeeType employeeType : employeeTypes) {
			employeeTypesMap.put(employeeType.getId(), employeeType.getName());
		}
		
		if (id != null) {
			employee = employeeService.findOne(id);
		}

		model.addAttribute("employee", employee);
		model.addAttribute("employeeTypes", employeeTypesMap);

		return "add-employee";

	}

	@PostMapping(value = "/save-employee")
	public String addEmployee(Model model, @ModelAttribute @Validated Employee employee, BindingResult result)
			throws Exception {

		log.debug("addEmployee method entered for name: " + employee.getFirstName() + " " + employee.getLastName());

		if (result.hasErrors()) {
			log.warn("addEmployee validation errors: " + result.getErrorCount());
			return "add-employee";
		}

		List<EmployeeType> employeeTypes = employeeTypeService.findAll();
		Map<Long, String> employeeTypesMap = new HashMap<Long, String>();
		
		for (EmployeeType employeeType : employeeTypes) {
			employeeTypesMap.put(employeeType.getId(), employeeType.getName());
		}
		
		Employee savedEmployee = employeeService.save(employee);
		
		model.addAttribute("employee", savedEmployee);
		model.addAttribute("employeeSaved", true);
		model.addAttribute("employeeTypes", employeeTypesMap);

		// action="/spring-mvc-java/addEmployee";

		// Object example = model;

		return "add-employee";

	}

}
