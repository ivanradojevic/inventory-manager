package org.zilker.inventorymanager.web.controller.rest.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.product.ProductType;
import org.zilker.inventorymanager.service.product.ProductTypeService;

@RestController
@RequestMapping(value = "/api/productTypes")
public class ProductTypeController {

	@Autowired
	private ProductTypeService productTypeService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProductType>> getProductTypes() {

		return new ResponseEntity<>(productTypeService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<ProductType> getProductTypeById(@PathVariable Long id) {
		ProductType productType = productTypeService.findOne(id);
		if (productType == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(productType, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ProductType> addProductType(@Validated @RequestBody ProductType newProductType) {

		ProductType savedProductType = productTypeService.save(newProductType);

		return new ResponseEntity<>(savedProductType, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<ProductType> deleteProductType(@PathVariable Long id) {
		ProductType deletedProductType = productTypeService.delete(id);

		if (deletedProductType == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(deletedProductType, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<ProductType> editProductType(@Validated @RequestBody ProductType productType,
			@PathVariable Long id) {

		if (!id.equals(productType.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		ProductType persistedproductType = productTypeService.save(productType);

		return new ResponseEntity<>(persistedproductType, HttpStatus.OK);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
