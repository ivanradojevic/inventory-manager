package org.zilker.inventorymanager.web.controller.example;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.zilker.inventorymanager.model.example.Example;

/**
 * @author imssbora
 */
@Component
public class ExampleValidator implements Validator {

   @Override
   public boolean supports(Class<?> clazz) {
      return Example.class.equals(clazz);
   }

   @Override
   public void validate(Object obj, Errors err) {

      ValidationUtils.rejectIfEmpty(err, "name", "example.name.empty", "Example name empty from validator");
    //  ValidationUtils.rejectIfEmpty(err, "email", "user.email.empty");
    //  ValidationUtils.rejectIfEmpty(err, "gender", "user.gender.empty");
   //   ValidationUtils.rejectIfEmpty(err, "languages", "user.languages.empty");

    //  Example user = (Example) obj;

   //   Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
        //    Pattern.CASE_INSENSITIVE);
//      if (!(pattern.matcher(user.getEmail()).matches())) {
//         err.rejectValue("email", "user.email.invalid");
//      }

   }

}