package org.zilker.inventorymanager.web.controller.rest.product;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.product.Product;
import org.zilker.inventorymanager.service.product.ProductService;

@RestController
@RequestMapping(value = "/api/products")
public class ProductContoller {
	
	private Logger log = LoggerFactory.getLogger(ProductContoller.class);

	@Autowired
	private ProductService productService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProducts() {

		return new ResponseEntity<>(productService.findAll(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Product> addProduct(@Validated @RequestBody Product newProduct) throws Exception {

		Product savedProduct = productService.save(newProduct);

		return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<Product> getProductById(@PathVariable Long id) {
		Product product = productService.findOne(id);
		if (product == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<Product> deleteProduct(@PathVariable Long id) {
		Product deleted = productService.delete(id);

		if (deleted == null) {
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(deleted, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<Product> editProduct(@Validated @RequestBody Product product, @PathVariable Long id)
			throws Exception {

		log.debug("edit product entered for id {}", product.getId());

		if (!id.equals(product.getId())) {
			log.warn("edit ids do not match: {}, {} ", id, product.getId());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Product persisted = productService.save(product);

		log.debug("edit product finished for id {}", product.getId());

		return new ResponseEntity<>(persisted, HttpStatus.OK);

	}
	

}
