package org.zilker.inventorymanager.web.controller.product;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.zilker.inventorymanager.web.controller.product.beans.ProductSearchBean;

@Component
public class ProductValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ProductSearchBean.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors err) {

//		ValidationUtils.rejectIfEmpty(err, "name", "product.name.empty", "Product name must not be empty");
//		ValidationUtils.rejectIfEmpty(err, "sN", "product.sN.empty", "Serial number must not be empty");
//		ValidationUtils.rejectIfEmpty(err, "pN", "product.pN.empty", "Product number must not be empty");
//		ValidationUtils.rejectIfEmpty(err, "manufacturerId", "product.manufacturerId.empty", "Manufacturer ID must not be empty");
//		ValidationUtils.rejectIfEmpty(err, "productTypeId", "product.productTypeId.empty", "ProductType ID must not be empty");

	}

}
