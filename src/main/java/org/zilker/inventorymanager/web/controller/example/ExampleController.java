package org.zilker.inventorymanager.web.controller.example;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.zilker.inventorymanager.model.example.Example;
import org.zilker.inventorymanager.service.example.ExampleService;

@Controller
public class ExampleController {

	@Autowired
	@Qualifier("exampleValidator")
	private Validator exampleValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(exampleValidator);
	}

	@Autowired
	private ExampleService exampleService;

	private Logger log = LoggerFactory.getLogger(ExampleController.class);

	@RequestMapping(value = "/examples")
	String getAllExamples(Model model) throws Exception {

		log.debug("getAllExamples method entered!");

		List<Example> examples = exampleService.findAll();
		model.addAttribute("examples", examples);

		log.debug("getAllExamples method finished!");

		return "examples";

	}

	@GetMapping(value = "/search")
	public String showExampleNameSearchForm(Model model, @RequestParam(value = "name", required = false) String name)
			throws Exception {

		log.debug("searchExample method entered!");

		Example example = new Example();

		model.addAttribute("example", example);

		return "search-example";

	}

	@GetMapping(value = "/search/{id}")
	public String showExampleDetail(Model model, @PathVariable Long id) throws Exception {

		log.debug("showExampleDetail method entered!");

		Example example = new Example();
		
		example = exampleService.findOne(id);

		model.addAttribute("example", example);

		log.debug("showExampleDetail method exit!");

		return "example-detail";

	}

	@RequestMapping(value = "/search-example-name")
	String getExamplesByName(Model model, @RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "ajax", required = false) String ajax) throws Exception {

		log.debug("getExamplesByName method entered!");

		List<Example> examples = exampleService.findAllByName(name);
		model.addAttribute("examples", examples);

		log.debug("getExamplesByName method finished!");

		String output = "true".equals(ajax) ? "examples-table-ajax" : "examples";

		return output;

	}

	@GetMapping(value = "/edit-example")
	public String showExampleForm(Model model, @RequestParam(value = "id", required = false) Long id) throws Exception {

		log.debug("addExample method entered!");

		// action="/spring-mvc-java/addEmployee";

		Example example = new Example();

		if (id != null) {
			example = exampleService.findOne(id);
		}

		model.addAttribute("example", example);

		return "add-example";

	}

	@PostMapping(value = "/save-example")
	public String addExample(Model model, @ModelAttribute @Validated Example example, BindingResult result)
			throws Exception {

		log.debug("addExample method entered for name: " + example.getName());

		if (result.hasErrors()) {
			log.warn("saveExample validation errors: " + result.getErrorCount());
			return "add-example";
		}

		Example savedExample = exampleService.save(example);
		model.addAttribute("example", savedExample);
		model.addAttribute("exampleSaved", true);

		// action="/spring-mvc-java/addEmployee";

		// Object example = model;

		return "add-example";

	}

	@GetMapping(value = "/edit-example/{id}")
	public String showEditFormModal(Model model, @ModelAttribute Example example, @PathVariable Long id) throws Exception {

		log.debug("addExampleGet method entered!");
		
		if(!id.equals(null))
		{
			example = exampleService.findOne(id);
		}
		
		model.addAttribute("example", example);
		
		
		log.debug("addExampleGet method exit!");

		return "example-add";

	}
	
	@GetMapping(value = "/add-example")
	public String showAddFormModal(Model model, @ModelAttribute Example example) throws Exception {

		log.debug("addExampleGet method entered!");
		
		model.addAttribute("example", example);
		
		
		log.debug("addExampleGet method exit!");

		return "example-add";

	}
	
	@PostMapping(value = "/example-save")
	public String addExampleFromModal(Model model, @ModelAttribute @Validated Example example, BindingResult result)
			throws Exception {

		log.debug("addExampleFromModal method entered for name: " + example.getName());

		if (result.hasErrors()) {
			log.warn("addExampleFromModal validation errors: " + result.getErrorCount());
			return "examples";
		}

		
		Example savedExample = exampleService.save(example);
		model.addAttribute("example", savedExample);
		model.addAttribute("exampleSaved", true);
		List<Example> examples = exampleService.findAll();
		model.addAttribute("examples", examples);
		log.debug("addExampleFromModal method exited for name: " + example.getName());
		return "examples";

	}
	
	@GetMapping(value = "/examples-ajax")
	public String examplesajax(Model model, @ModelAttribute Example example) throws Exception {

		return "examples-ajax";

	}

	@GetMapping(value = "/delete-example/{id}")
	public String showDeleteFormModal(Model model, @ModelAttribute Example example, @PathVariable Long id) throws Exception {

		log.debug("showDeleteFormModal method entered!");
		
		if(!id.equals(null))
		{
			example = exampleService.findOne(id);
		}
		
		model.addAttribute("example", example);
		
		
		log.debug("showDeleteFormModal method exit!");

		return "delete-example";

	}
	
	@PostMapping(value = "/delete-example")
	public String deleteExampleFromModal(Model model, @ModelAttribute @Validated Example example, BindingResult result)
			throws Exception {

		log.debug("deleteExampleFromModal method entered for name: " + example.getName());

		if (result.hasErrors()) {
			log.warn("saveExample validation errors: " + result.getErrorCount());
			return "examples";
		}

		
		Example deletedExample = exampleService.delete(example.getId());
		model.addAttribute("example", deletedExample);
		model.addAttribute("exampleDeleted", true);
		List<Example> examples = exampleService.findAll();
		model.addAttribute("examples", examples);
		log.debug("deleteExampleFromModal method exited for name: " + example.getName());
		return "examples";

	}
}