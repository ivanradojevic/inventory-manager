package org.zilker.inventorymanager.web.controller.product.beans;

public class ProductSearchBean {
	
	public ProductSearchBean() {
	}
	
	public ProductSearchBean(String term) {
		super();
		this.term = term;
	}

	String term;

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	
	
}
