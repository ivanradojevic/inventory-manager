package org.zilker.inventorymanager.web.controller.rest.employee;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.zilker.inventorymanager.model.employee.EmployeeType;
import org.zilker.inventorymanager.service.employee.EmployeeTypeService;

@RestController
@RequestMapping(value = "/api/employeeType")
public class EmployeeTypeRestController {

	private Logger log = LoggerFactory.getLogger(EmployeeTypeRestController.class);

	@Autowired
	private EmployeeTypeService employeeTypeService;

	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<EmployeeType>> getAllEmployeeTypes() {

		log.debug("getAllEmployeeTypes method entered!");

		List<EmployeeType> employeeTypes = employeeTypeService.findAll();

		log.debug("getAllEmployeeTypes method finished!");

		return new ResponseEntity<>(employeeTypes, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<EmployeeType> getEmployeeTypeById(@PathVariable Long id) {

		log.debug("getEmployeeTypeById method entered for id {}!", id);

		EmployeeType employeeType = employeeTypeService.findOne(id);

		if (employeeType == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		log.debug("getEmployeeTypeById method finished for id {}!", id);

		return new ResponseEntity<>(employeeType, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<EmployeeType> deleteEmployeeType(@PathVariable Long id) {

		log.debug("deleteEmployeeType method entered for id {}!", id);

		EmployeeType deletedType = employeeTypeService.delete(id);

		if (deletedType == null)

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		log.debug("deleteEmployeeType method finished for id {}!", id);

		return new ResponseEntity<>(deletedType, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<EmployeeType> addEmployeeType(@Validated @RequestBody EmployeeType employeeType) {

		log.debug("addEmployeeType entered for employeeType {}!", employeeType.getName());

		EmployeeType employeeType2 = employeeTypeService.save(employeeType);

		log.debug("addEmployeeType finished for employeeType {}!", employeeType.getName());

		return new ResponseEntity<>(employeeType2, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<EmployeeType> edit(@Validated @RequestBody EmployeeType employeeType, @PathVariable Long id) {

		log.debug("editEmployeeType entered for employeeType{}!", employeeType.getId());

		if (!id.equals(employeeType.getId())) {
			log.warn("ids do not match {} and {}!", employeeType.getId(), id);
			return new ResponseEntity<EmployeeType>(HttpStatus.BAD_REQUEST);
		}
		EmployeeType persistedEmployeeType = employeeTypeService.save(employeeType);

		log.debug("editEmployeeType finished for employeeType {}!", employeeType.getId());

		return new ResponseEntity<EmployeeType>(persistedEmployeeType, HttpStatus.OK);
	}

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Exception> handle(Exception e) {
		log.error("Unexpected error occurred: ", e);

		// TODO
		// create error response object, instead of exception
		return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
